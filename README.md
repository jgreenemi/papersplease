# Papers, Please

This is a Turing machine emulation taking on the theme of the game `Papers, Please`. 

## Definition of a Turing Machine, In Brief

- Tape, containing cells. Each cell contains an integer, which is an encoding of a letter in a finite alphabet.
- Instruction table.
- State registry, with finite states. Among these states, `START`.
- Head, for I/O and moving left/right on the tape.

