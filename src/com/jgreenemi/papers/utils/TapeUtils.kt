package com.jgreenemi.papers.utils

import com.jgreenemi.papers.types.Tape


fun makeTape(kind: List<Int>, count: Int): Tape {
    val list = mutableListOf<Int>()
    for (i in 0..count) list.add(kind.random())
    return Tape(list)
}