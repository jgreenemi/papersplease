package com.jgreenemi.papers

import com.jgreenemi.papers.types.Machine
import com.jgreenemi.papers.utils.makeTape

fun main() {
    // val tape = makeTape(0, 10000)  // Negative check.
    val tape = makeTape(listOf(1, 2, 3, 4, 0), 10)
    val m = Machine(acceptanceLimit = 100)
    val finalState = m.runTape(tape)
    println("Received $finalState from Machine with ${m.getAcceptanceHistory()} acceptance(s).")
}