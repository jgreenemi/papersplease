package com.jgreenemi.papers.types

enum class Direction {
    LEFT, RIGHT
}

enum class Decision {
    ACCEPT, REJECT
}

enum class State {
    START, PROCESSING, HALT
}

/**
 * tapePosition: Starts at -1 because the Head hasn't moved onto the tape yet,
 *   when we've just started it up.
 */
class Head(
        var tapePosition: Int = -1,
        var nextDirection: Direction = Direction.RIGHT) {

    /**
     * If the machine does not process an infinite amount of tape, we should be
     * able to handle a situation where the tape didn't have enough Accepts to
     * breach the acceptanceLimit, and has run out of cells to process (Head has
     * gone out of bounds). Note this does not check for having moved left (to a
     * -1 position) on the tape, as this implementation only expects rightward
     * movements.
     */
    fun getHasNext(tapeEnd: Int, debug: Boolean = false): Boolean {
        tapePosition += 1
        val hasNext = tapePosition < tapeEnd
        if (debug) println("getHasNext: $hasNext from ($tapePosition < $tapeEnd)")
        return hasNext
    }
}

data class Tape(val cells: List<Int>) {
    val end = cells.size
}

class Machine(
        val head: Head = Head(),
        val acceptanceLimit: Int = 3) {
    private var acceptanceHistory: Int = 0
    private var state = State.START

    private fun getState(): State {
        return this.state
    }

    private fun decide(cell: Int): Decision {
        return when (cell) {
            0 -> Decision.REJECT
            else -> Decision.ACCEPT
        }
    }

    private fun getNextState(previousDecision: Decision): State {
        if (previousDecision == Decision.ACCEPT) {
            acceptanceHistory += 1
        }

        if (acceptanceHistory >= acceptanceLimit) {
            println("Gate closing - Arstotzka is accepting no more immigrants today.")
            return State.HALT
        }
        return State.PROCESSING
    }

    fun runTape(tape: Tape): State {
        println("Gate open - Arstotzka is now accepting immigrant applications.")
        while (this.state != State.HALT) {
            val headHasNext = head.getHasNext(tape.end)
            if (headHasNext) {
                println("\tJacksepticeye: `Papers, please!`")
                val cell = tape.cells[head.tapePosition]
                val decision = decide(cell)
                println("Decision on immigrant application #${head.tapePosition}: $cell: $decision")
                this.state = getNextState(decision)
            } else {
                // Head has no next - tape has ended.
                this.state = State.HALT
            }
        }
        return this.state
    }

    fun getAcceptanceHistory(): Int {
        return acceptanceHistory
    }
}